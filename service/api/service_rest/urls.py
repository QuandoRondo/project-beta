from django.urls import path

from .views import api_appointment_cancel, api_technician_list, api_appointment_list, api_appointment_finish,api_show_history

urlpatterns = [
    path("technicians/", api_technician_list, name='api_technician_list'),
    path("appointments/", api_appointment_list, name='api_appointment_list'),
    path("appointments/<int:id>/finish/", api_appointment_finish, name='api_appointment_finish'),
    path("appointments/<int:id>/cancel/", api_appointment_cancel, name='api_appointment_cancel'),
    path("servicehistory/", api_show_history, name='api_show_history')
]
