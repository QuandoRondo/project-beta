import React, { useState } from 'react';

const ManufacturerForm =()=> {

    const [formData, setFormData] = useState({
        name: '',
    });

    const handleSubmit = async (event) => {
        event.preventDefault();

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const response = await fetch(manufacturerUrl, fetchConfig);

            if (response.ok) {
                setFormData({
                    name: '',
                });

                alert('Manufacturer Added');
            } else {
                const data = await response.json();
                alert(`Error: ${data.detail}`);
            }
        } catch (error) {
            console.error('Error submitting form:', error);
        }
    };

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleFormChange}
                                value={formData.name}
                                placeholder="Manufacturer Name"
                                required
                                type="text"
                                name="name"
                                id="name"
                                className="form-control"
                            />
                            <label htmlFor="name">Manufacturer Name</label>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm;
