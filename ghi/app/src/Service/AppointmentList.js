import React, { useState, useEffect } from "react";

const AppointmentList = () => {
  const [appointments, setAppointments] = useState([]);

  const finished = async (id) => {
    const response = await fetch(
      `http://localhost:8080/api/appointments/${id}/finish/`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    setAppointments(
      appointments.filter((appointment) => appointment.id !== id)
    );
  };

  const canceled = async (id) => {
    await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    });

    setAppointments(
      appointments.filter((appointment) => appointment.id !== id)
    );
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("http://localhost:8080/api/appointments/");
        if (response.ok) {
          const data = await response.json();
          setAppointments(data.appointments);
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, []);
  return (
    <div>
      <h1>Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody className="table-group-divider">
          {appointments.map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.vip ? "Yes" : "No"}</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date}</td>
                <td>{appointment.time}</td>
                <td>
                  {appointment.technician.first_name}{" "}
                  {appointment.technician.last_name}
                </td>
                <td>{appointment.reason}</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => canceled(appointment.id)}
                  >
                    Cancel
                  </button>
                  <button
                    className="btn btn-success"
                    onClick={() => finished(appointment.id)}
                  >
                    Finish
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
export default AppointmentList;
