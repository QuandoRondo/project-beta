import React, { useState, useEffect } from "react";



const ManufacturerList = () => {
    const [manufacturers, setManufacturers] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('http://localhost:8100/api/manufacturers/')
                if (response.ok){
                    const data = await response.json();

                    setManufacturers(data.manufacturers);
                }
            } catch(error){
                console.error('Error fetching data:', error)
            }
        }
        fetchData();
    }, [])



    return (
        <div className="container mt-5 pt-1">
            <div className="mt-5">
                <h1 className="text-center mb-3">Manufacturers</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody className="table-group-divider">
                        {manufacturers.map((manufacturer) => {
                            return (
                                <tr key={manufacturer.id}>
                                    <td>{manufacturer.name}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>

    )
}
export default ManufacturerList
