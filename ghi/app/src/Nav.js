import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <div className="row">
            {/* Sales Column */}
            <div className="col">
              <ul className="navbar-nav">
                <li className="nav-item"><NavLink className="nav-link" to="create">Add a Sales Person</NavLink></li>
                <li className="nav-item"><NavLink className="nav-link" to="salespeople">Sales People List</NavLink></li>
                <li className="nav-item"><NavLink className="nav-link" to="salesform">Add a Sale</NavLink></li>
                <li className="nav-item"><NavLink className="nav-link" to="saleslist">Sales List</NavLink></li>
                <li className="nav-item"><NavLink className="nav-link " aria-current="page" to="salespersonhistory">SalesPerson History</NavLink></li>
              </ul>
            </div>

            {/* Customers Column */}
            <div className="col">
              <ul className="navbar-nav">
                <li className="nav-item"><NavLink className="nav-link" to="addcustomer">Add a Customer</NavLink></li>
                <li className="nav-item"><NavLink className="nav-link" to="customerlist">Customer List</NavLink></li>
              </ul>
            </div>

            {/* Technicians Column */}
            <div className="col">
              <ul className="navbar-nav">
                <li className="nav-item"><NavLink className="nav-link" to="/technicians">Technicians</NavLink></li>
                <li className="nav-item"><NavLink className="nav-link" to="/technicians/new">Create Technician</NavLink></li>
              </ul>
            </div>

            {/* Automobiles Column */}
            <div className="col">
              <ul className="navbar-nav">
                <li className="nav-item"><NavLink className="nav-link" to="/automobiles">Automobiles</NavLink></li>
                <li className="nav-item"><NavLink className="nav-link" to="/automobiles/new">Create an Automobile</NavLink></li>
              </ul>
            </div>

            {/* Appointments Column */}
            <div className="col">
              <ul className="navbar-nav">
                <li className="nav-item"><NavLink className="nav-link" to="/appointments">Appointments</NavLink></li>
                <li className="nav-item"><NavLink className="nav-link" to="/appointments/new">Create Appointment</NavLink></li>
                <li className="nav-item"><NavLink className="nav-link" to="/appointments/servicehistory">Service History</NavLink></li>
              </ul>
            </div>

            {/* Manufacturers Column */}
            <div className="col">
              <ul className="navbar-nav">
                <li className="nav-item"><NavLink className="nav-link" to="manufacturers">Manufacturers</NavLink></li>
                <li className="nav-item"><NavLink className="nav-link" to="/manufacturers/create">Create a Manufacturer</NavLink></li>
              </ul>
            </div>

            {/* Models Column */}
            <div className="col">
              <ul className="navbar-nav">
                <li className="nav-item"><NavLink className="nav-link" to="/models">Models</NavLink></li>
                <li className="nav-item"><NavLink className="nav-link" to="/models/new">Create a Model</NavLink></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
