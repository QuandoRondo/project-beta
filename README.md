# CarCar

Team:

* Alexander Wilson - Sales
* Sherika Fayson - Service


## How To Start
Fork this repository


Clone the forked repository onto your local computer:
git clone https://gitlab.com/QuandoRondo/project-beta.git


Build and run the project using Docker with these commands:



docker volume create beta-data
docker-compose build
docker-compose up




After running these commands, make sure all of your Docker containers are running


View the project in the browser: http://localhost:3000/


## Design


![Alt text](https://gitlab.com/QuandoRondo/project-beta/-/raw/main/Diagram.png?ref_type=heads "CarCar Value Object Design")

## Service microservice

### Technician
This model represents the service technicians. Each technician is uniquely identified by their employee ID and has personal details like first name and last name. The model is for use in scheduling service appointments.

### AutomobileVO

The AutomobileVO model is dedicated to tracking vehicles. It uniquely identifies each vehicle by a VIN (Vehicle Identification Number) and records whether the vehicle has been sold.

### Appointment
Central to the application, the Appointment model organizes service appointments. It captures essential details such as the date and time of the appointment, the reason for the appointment, and its current status (e.g., scheduled, finished, canceled). Each appointment is linked to a specific vehicle (through VIN) and customer. Importantly, the model associates each appointment with a designated technician and includes information about whether the appointment is considered VIP, indicating higher priority or special treatment.

## RESTful API (Port 8080)
**Action**|**        Method**|**        URL**
:-----:|:-----:|:-----:
List technicians        |GET        |http://localhost:8080/api/technicians/
Create a technician|POST        |http://localhost:8080/api/technicians/
List appointments|GET        |http://localhost:8080/api/appointments/
Create an appointment|POST|http://localhost:8080/api/appointments/
Set appointment status to "canceled"|PUT|http://localhost:8080/api/appointments/:id/cancel/
Set appointment status to "finished"|PUT|http://localhost:8080/api/appointments/:id/finish/


## Sales microservice


The AutomobileVO model interfaces directly with the Inventory model through a polling mechanism. This process facilitates the creation of AutomobileVO instances within the Inventory microservice. These instances are crucial as they provide specific vin and href properties of an automobile, which are subsequently utilized by the Customer or Salesperson models to facilitate the execution of sales transactions.

The Salesperson model is designed with essential attributes, including the name of the salesperson and an employee identification number. This model plays a pivotal role in tracking and managing salesperson-related information.

The Customer model is structured to store key customer details, encompassing the customer's name, address, and contact phone number. This information is vital for maintaining comprehensive customer records.

Lastly, the Sale model is a comprehensive construct that includes several relational properties: it references the Salesperson model via a foreign key to the salesperson, the Customer model via a foreign key to the customer, and the AutomobileVO model for the automobile details. Additionally, it contains a price property, capturing the financial aspect of the sale. This model is integral in encapsulating the complete details of each sales transaction.

## Sales API



 List Salespeople               |GET| http://localhost:8090/api/salespeople/


 Create a Salesperson           |POST| http://localhost:8090/api/salespeople/


 Delete a Specific Salesperson  |DELETE| http://localhost:8090/api/salespeople/:id/|


 List Customers                 |GET| http://localhost:8090/api/customers/


 Create a Customer              |POST| http://localhost:8090/api/customers/


 Delete a Specific Customer     |DELETE| http://localhost:8090/api/customers/:id/


 List Sales                     |GET| http://localhost:8090/api/sales/


 Create a Sale                  |POST| http://localhost:8090/api/sales/


 Delete a Specific Sale         |DELETE| http://localhost:8090/api/sales/:id/
